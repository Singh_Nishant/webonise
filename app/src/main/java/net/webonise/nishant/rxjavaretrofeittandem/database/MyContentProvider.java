package net.webonise.nishant.rxjavaretrofeittandem.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import static net.webonise.nishant.rxjavaretrofeittandem.database.SuggestionsDatabase.TABLE_SUGGESTION;

public class MyContentProvider extends ContentProvider {


    // database
    private SuggestionsDatabase suggestionsDatabase;

    private static final String AUTHORITY = "net.webonise.nishant.rxjavaretrofeittandem.provider";
    //private static final String BASE_PATH = "suggestion";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + SuggestionsDatabase.TABLE_SUGGESTION);


    public MyContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        SQLiteDatabase sqlDB = suggestionsDatabase.getWritableDatabase();
        long id = 0;
        id = sqlDB.insert(SuggestionsDatabase.TABLE_SUGGESTION, null, values);
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(CONTENT_URI + "/" + id);

    }

    @Override
    public boolean onCreate() {

        suggestionsDatabase = new SuggestionsDatabase(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // Set the table
        queryBuilder.setTables(TABLE_SUGGESTION);

        SQLiteDatabase db = suggestionsDatabase.getWritableDatabase();

        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;

    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
