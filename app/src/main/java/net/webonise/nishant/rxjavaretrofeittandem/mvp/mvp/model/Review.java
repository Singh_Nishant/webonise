
package net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Review implements Serializable {

    @SerializedName("aspects")
    private List<Aspect> mAspects;
    @SerializedName("author_name")
    private String mAuthorName;
    @SerializedName("author_url")
    private String mAuthorUrl;
    @SerializedName("language")
    private String mLanguage;
    @SerializedName("profile_photo_url")
    private String mProfilePhotoUrl;
    @SerializedName("rating")
    private Long mRating;
    @SerializedName("text")
    private String mText;
    @SerializedName("time")
    private Long mTime;

    public List<Aspect> getAspects() {
        return mAspects;
    }

    public void setAspects(List<Aspect> aspects) {
        mAspects = aspects;
    }

    public String getAuthorName() {
        return mAuthorName;
    }

    public void setAuthorName(String author_name) {
        mAuthorName = author_name;
    }

    public String getAuthorUrl() {
        return mAuthorUrl;
    }

    public void setAuthorUrl(String author_url) {
        mAuthorUrl = author_url;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

    public String getProfilePhotoUrl() {
        return mProfilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profile_photo_url) {
        mProfilePhotoUrl = profile_photo_url;
    }

    public Long getRating() {
        return mRating;
    }

    public void setRating(Long rating) {
        mRating = rating;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public Long getTime() {
        return mTime;
    }

    public void setTime(Long time) {
        mTime = time;
    }

}
