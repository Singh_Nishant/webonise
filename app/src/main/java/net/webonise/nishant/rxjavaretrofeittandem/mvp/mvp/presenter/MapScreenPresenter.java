package net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.presenter;

import android.graphics.Bitmap;
import android.util.Log;

import net.webonise.nishant.rxjavaretrofeittandem.api.IPlaceService;
import net.webonise.nishant.rxjavaretrofeittandem.api.ServiceFactory;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.PlaceDetailsPojo;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.view.IMapView;
import net.webonise.nishant.rxjavaretrofeittandem.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by NISHANT on 13-11-2016.
 */

public class MapScreenPresenter {
    IMapView mMapView;


    public MapScreenPresenter(IMapView imap) {
        mMapView = imap;
    }


    public void onMarkerClicked(String PlaceID) {
        Log.e("test", "placeId" + PlaceID);
        mMapView.showProgressDialog();
        fetchPlaceDetails(PlaceID);
    }

    public void fetchPlaceDetails(String placeID) {
        IPlaceService service = ServiceFactory.createRetrofitService(IPlaceService.class, IPlaceService.PLACE_ENDPOINT);


        Subscription subscription = service.getPlaceDetails(placeID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PlaceDetailsPojo>() {
                    @Override
                    public final void onCompleted() {
                        // do nothing
                    }

                    @Override
                    public final void onError(Throwable e) {
                        Log.e("nishant", e.getMessage());
                        mMapView.cancelProgressDialog();
                    }

                    @Override
                    public final void onNext(PlaceDetailsPojo response) {
                        mMapView.cancelProgressDialog();
                        if (response != null & response.getStatus().equalsIgnoreCase(Constants.OK)) {
                            if (response.getResult().getPhotos() != null & response.getResult().getPhotos().size() > 0) {
                                preparePhotoUrl(response);
                            } else {
                                Log.e("test", "photo is null");
                            }
                        } else {
                            mMapView.showToast(response.getStatus().toString());
                        }


                    }
                });

    }

    private void preparePhotoUrl(PlaceDetailsPojo response) {
        List<String> mPhotoUrl = new ArrayList<String>();
        for (int i = 0; i < response.getResult().getPhotos().size(); i++) {
            String photoRefrence = response.getResult().getPhotos().get(i).getPhotoReference();
            String url = IPlaceService.PLACE_ENDPOINT + "/api/place/photo?maxwidth=200&photoreference=" + photoRefrence + "&key=AIzaSyCWly2FtZjMH_b2qGF02w9lJ1_PIxaoSZg";
            mPhotoUrl.add(url);

        }
        mMapView.setPlaceImage(mPhotoUrl);


    }

    // This constructs an `Observable` to download the image
    public Observable<Bitmap> getImageNetworkCall() {
        // Insert network call here!
        Bitmap bit = null;
        return Observable.just(bit);
    }


    Subscription subscription = getImageNetworkCall()
            // Specify the `Scheduler` on which an Observable will operate
            .subscribeOn(Schedulers.io())
            // Specify the `Scheduler` on which a subscriber will observe this `Observable`
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<Bitmap>() {

                // This replaces `onPostExecute(Bitmap bitmap)`
                @Override
                public void onNext(Bitmap bitmap) {
                    // Handle result of network request
                }

                @Override
                public void onCompleted() {
                    // Update user interface if needed
                }

                @Override
                public void onError(Throwable e) {

                }


            });


}
