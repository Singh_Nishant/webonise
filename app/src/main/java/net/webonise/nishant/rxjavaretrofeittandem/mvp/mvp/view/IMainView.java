package net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.view;

import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.PlacePojo;

/**
 * Created by NISHANT on 30-10-2016.
 */

public interface IMainView {
    public void loadPlaces(PlacePojo data);

    public void showToast(String toast);

    public void moveToMapActivity();
}
