
package net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Aspect implements Serializable{

    @SerializedName("rating")
    private Long mRating;
    @SerializedName("type")
    private String mType;

    public Long getRating() {
        return mRating;
    }

    public void setRating(Long rating) {
        mRating = rating;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

}
