package net.webonise.nishant.rxjavaretrofeittandem.api;

import retrofit.RestAdapter;

/**
 * Created by NISHANT on 30-10-2016.
 */

public class ServiceFactory {

   public static <T> T createRetrofitService(final Class<T> clazz, final String endPoint) {
        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(endPoint)
                .build();
        T service = restAdapter.create(clazz);

        return service;
    }


}
