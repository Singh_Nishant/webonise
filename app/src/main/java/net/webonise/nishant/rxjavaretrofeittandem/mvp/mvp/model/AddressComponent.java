
package net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AddressComponent implements Serializable {

    @SerializedName("long_name")
    private String mLongName;
    @SerializedName("short_name")
    private String mShortName;
    @SerializedName("types")
    private List<String> mTypes;

    public String getLongName() {
        return mLongName;
    }

    public void setLongName(String long_name) {
        mLongName = long_name;
    }

    public String getShortName() {
        return mShortName;
    }

    public void setShortName(String short_name) {
        mShortName = short_name;
    }

    public List<String> getTypes() {
        return mTypes;
    }

    public void setTypes(List<String> types) {
        mTypes = types;
    }

}
