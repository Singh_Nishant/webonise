package net.webonise.nishant.rxjavaretrofeittandem.api;

import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.PlacePojo;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by NISHANT on 13-11-2016.
 */

public interface IPlaceDetailsService {
    String PLACE_ENDPOINT = "https://maps.googleapis.com/maps";

    // https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJIanf-QPBwjsRMcoPLIggqxM&key=AIzaSyCWly2FtZjMH_b2qGF02w9lJ1_PIxaoSZg
    @GET("/api/place/details/json?key=AIzaSyCWly2FtZjMH_b2qGF02w9lJ1_PIxaoSZg")
    Observable<PlacePojo> getPlaceDetails(@Query("placeid") String placeId);

}
