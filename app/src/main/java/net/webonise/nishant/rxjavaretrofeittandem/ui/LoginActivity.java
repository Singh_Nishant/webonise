package net.webonise.nishant.rxjavaretrofeittandem.ui;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.squareup.picasso.Picasso;

import net.webonise.nishant.rxjavaretrofeittandem.R;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends AppCompatActivity {
    LoginButton loginButton;
    String TAG = LoginActivity.class.getSimpleName();
    public static CallbackManager callbackmanager;
    Profile profile;
    private ImageView profile_pic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        callbackmanager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        profile_pic = (ImageView) findViewById(R.id.profile_pic);
        loginButton.setReadPermissions("public_profile", "email", "user_friends");


        if (isFacebookLoggedIn()) {

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);

        }


        loginButton.registerCallback(callbackmanager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult result) {

                Bundle params = new Bundle();
                params.putString("fields", "id,email,gender,cover,picture.type(large)");
                new GraphRequest(AccessToken.getCurrentAccessToken(), "me", params, HttpMethod.GET,
                        new GraphRequest.Callback() {
                            @Override
                            public void onCompleted(GraphResponse response) {
                                if (response != null) {
                                    try {
                                        JSONObject data = response.getJSONObject();
                                        if (data.has("picture")) {
                                            String profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");
                                            //Bitmap profilePic = BitmapFactory.decodeStream(profilePicUrl.openConnection().getInputStream());

                                            Picasso.with(LoginActivity.this)
                                                    .load(profilePicUrl)
                                                    .placeholder(net.webonise.nishant.rxjavaretrofeittandem.R.drawable.image_laoding)
                                                    .error(net.webonise.nishant.rxjavaretrofeittandem.R.drawable.image_error_loading)
                                                    .resize(200, 200)
                                                    .centerCrop()
                                                    .into(profile_pic);

                                            //profile_pic.setBitmap(profilePic);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }).executeAsync();
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);

                // you can make a new graph request for user info here
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Logging in canceled.", Toast.LENGTH_SHORT).show();
                Log.i(TAG, "Facebook login canceled.");
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), "Error occurred while logging in. Please try again.", Toast.LENGTH_SHORT).show();
                Log.i(TAG, "Facebook login error.");
            }
        });


        findKeyHash();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        callbackmanager.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "Performed Successfully");


    }

    private void findKeyHash() {
        try {

            PackageInfo info = getPackageManager().getPackageInfo("net.webonise.nishant.rxjavaretrofeittandem", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e(TAG, Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }

        } catch (PackageManager.NameNotFoundException e) {


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    public boolean isFacebookLoggedIn() {
        return AccessToken.getCurrentAccessToken() != null;
    }

}
