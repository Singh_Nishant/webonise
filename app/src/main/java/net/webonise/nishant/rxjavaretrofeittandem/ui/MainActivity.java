package net.webonise.nishant.rxjavaretrofeittandem.ui;

import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.webonise.nishant.rxjavaretrofeittandem.Listeners.RecyclerItemClickListener;
import net.webonise.nishant.rxjavaretrofeittandem.R;
import net.webonise.nishant.rxjavaretrofeittandem.adapter.SampleAdapter;
import net.webonise.nishant.rxjavaretrofeittandem.adapter.SuggestionSimpleCursorAdapter;
import net.webonise.nishant.rxjavaretrofeittandem.application.MyApplication;
import net.webonise.nishant.rxjavaretrofeittandem.database.MyContentProvider;
import net.webonise.nishant.rxjavaretrofeittandem.database.SuggestionsDatabase;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.PlacePojo;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.Result;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.presenter.MainPresenter;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.view.IMainView;
import net.webonise.nishant.rxjavaretrofeittandem.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static net.webonise.nishant.rxjavaretrofeittandem.database.SuggestionsDatabase.FIELD_SUGGESTION;

public class MainActivity extends AppCompatActivity implements IMainView, SearchView.OnQueryTextListener, SearchView.OnSuggestionListener, LoaderManager.LoaderCallbacks<Cursor> {

    //Binding view
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    SearchView mSearchView;
    TextView tvMessage;
    private SuggestionsDatabase database;
    PlacePojo mplacePojo;
    RecyclerView.Adapter mCardAdapter;
    List<Result> mList;
    ProgressDialog progressDialog;
    private MenuItem searchMenuItem;
    //Interface between view and model. It has both View and model object
    MainPresenter presenter;
    private Tracker mTracker;
    SuggestionSimpleCursorAdapter simpleAdapter;
    //Cursor cursor=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        MyApplication application = (MyApplication) getApplication();
        mTracker = application.getDefaultTracker();
        Log.e("test", "oncreate called again");

        presenter = new MainPresenter(this);

        mList = new ArrayList<Result>();
        database = new SuggestionsDatabase(this);


        String[] columns = new String[]{SuggestionsDatabase.FIELD_SUGGESTION};
        int[] columnTextId = new int[]{android.R.id.text1};
        simpleAdapter = new SuggestionSimpleCursorAdapter(getBaseContext(),
                R.layout.suggestion_row, null,
                columns, columnTextId
                , 0);


        getLoaderManager().initLoader(0, null, this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        searchMenuItem = menu.findItem(R.id.action_search);

        mSearchView = (SearchView) searchMenuItem.getActionView();
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnSuggestionListener(this);
        mSearchView.setQueryHint("Find places");
        mSearchView.setSuggestionsAdapter(simpleAdapter);

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Set styles for expanded state here
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.RED));
                }
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Set styles for collapsed state here
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
                }
                return true;
            }
        });


        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (presenter != null)
                //         loadPlaces(presenter.mResponse);
                Log.e("test", "LANDSCAPE");
        } else {
            if (presenter != null)
                //       loadPlaces(presenter.mResponse);
                Log.e("test", "PORTRAIT");
        }


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    }


    private void initRecyclerView() {
        recyclerView.setVisibility(View.VISIBLE);
        tvMessage.setVisibility(View.GONE);
        mCardAdapter = new SampleAdapter(this, mList);

        // GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        // gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        // recyclerView.setLayoutManager(gridLayoutManager);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mCardAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        if (mList != null & mList.size() > 0) {
                            String place_id = mList.get(position).getPlaceId();

                            presenter.moveToMapActivity();
                        }


                    }
                })
        );

    }


    private void showDialog() {
        progressDialog = ProgressDialog.show(this, "Loader", "Loading", true);
    }


    @Override
    public void loadPlaces(PlacePojo response) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if (response != null && response.getStatus().equalsIgnoreCase("ok")) {
            mList = response.getResults();
            mplacePojo = response;
            initRecyclerView();
        } else {
            Toast.makeText(getApplicationContext(), "TimeOut ", Toast.LENGTH_LONG).show();
        }


    }


    @Override
    public void showToast(String toast) {
        Toast.makeText(getApplicationContext(), toast, Toast.LENGTH_LONG);
    }


    @Override
    public void moveToMapActivity() {
        Intent intent = new Intent(MainActivity.this, MapActivity.class);

        intent.putExtra(Constants.POJO_CLASS, mplacePojo);
        startActivity(intent);
    }


    private long insertSuggestion(String query) {
        ContentValues cv = new ContentValues();
        cv.put(FIELD_SUGGESTION, query);
        Uri uri = getContentResolver().insert(MyContentProvider.CONTENT_URI, cv);
        Log.e("test", "Nishant---URI" + uri.toString());
        return 0;
    }


    @Override
    public boolean onQueryTextChange(String newText) {

        // getLoaderManager().restartLoader(0, null, this);
        // getLoaderManager().initLoader(0, null, this);
        return false;

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        hideSoftKey();
        long result = 0;
        try {
            result = insertSuggestion(query);


        } catch (Exception e) {
            Log.e("test", e.getMessage());
        }
        if (result == 0) {
            Log.e("test", " query insrted in database");
        }


        showDialog();

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Search")
                .build());

        //Debug.startMethodTracing("myapp");
        presenter.fetchPlaces(query);
        //Debug.stopMethodTracing();
        return false;
    }


    private void hideSoftKey() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mSearchView.getWindowToken(), 0);
        mSearchView.clearFocus();
    }

    @Override
    public boolean onSuggestionSelect(int position) {
        return false;
    }

    @Override
    public boolean onSuggestionClick(int position) {


        Cursor cursor = (Cursor) mSearchView.getSuggestionsAdapter().getItem(position);
        int indexColumnSuggestion = cursor.getColumnIndex(FIELD_SUGGESTION);

        mSearchView.setQuery(cursor.getString(indexColumnSuggestion), false);

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.e("test", "Ondestroey");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("test", "OnStop");
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Log.e("test", "OncreateLoader 0");
        String[] projection = {SuggestionsDatabase.FIELD_ID, SuggestionsDatabase.FIELD_SUGGESTION};
        Cursor cursor = getContentResolver().query(MyContentProvider.CONTENT_URI, projection, null, null, null);
        CursorLoader cursorLoader = new CursorLoader(MainActivity.this, MyContentProvider.CONTENT_URI, projection, null, null, null);
        Log.e("test", "OncreateLoader 1");
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        Log.e("test", "OnLoadfinished 0");
        if (cursor != null && cursor.getCount() > 0) {
            Log.e("test", "OnLoadfinished 1");
            (simpleAdapter).
                    swapCursor(cursor);
        }

    }


    @Override
    public void onLoaderReset(Loader loader) {
        Log.e("test", "onLoaderReset 0");
        (simpleAdapter).
                swapCursor(null);
    }

    public void gitTest() {
        Log.e("test", "test");
    }
}
