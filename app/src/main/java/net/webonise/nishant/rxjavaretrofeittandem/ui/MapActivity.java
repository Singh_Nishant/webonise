package net.webonise.nishant.rxjavaretrofeittandem.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.PlaceDetailsPojo;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.PlacePojo;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.presenter.MapScreenPresenter;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.view.IMapView;
import net.webonise.nishant.rxjavaretrofeittandem.utils.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MapActivity extends AppCompatActivity implements IMapView, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, LocationListener {

    @Bind(net.webonise.nishant.rxjavaretrofeittandem.R.id.tvDummy)
    TextView tvDummy;

    @Bind(net.webonise.nishant.rxjavaretrofeittandem.R.id.linearLayout)
    LinearLayout linearLayout;

    private GoogleMap mSupportMap;
    private Marker mMarker;
    private String mPlaceId;
    private PlacePojo mPlacePojo;
    private MapScreenPresenter mapScreenPresenter;
    ProgressDialog progressDialog;
    private boolean canGetLocation;

    public boolean isGPSEnabled = false;

    private Location location;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

    protected LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(net.webonise.nishant.rxjavaretrofeittandem.R.layout.activity_map);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(net.webonise.nishant.rxjavaretrofeittandem.R.id.toolbar);

        setSupportActionBar(toolbar);
        mPlaceId = getIntent().getExtras().getString(Constants.PLACE_ID);
        mPlacePojo = (PlacePojo) getIntent().getSerializableExtra(Constants.POJO_CLASS);


        mapScreenPresenter = new MapScreenPresenter(this);

        // map settings
        FragmentManager fmanager = getSupportFragmentManager();
        SupportMapFragment supportmapfragment = (SupportMapFragment) fmanager.findFragmentById(net.webonise.nishant.rxjavaretrofeittandem.R.id.map);
        supportmapfragment.getMapAsync(this);


    }

    HashMap<String, String> markerHashMap = new HashMap<String, String>();

    private void setImages(final List<String> urlList) {
        Log.e("test", "size of photo list" + urlList.size());
        tvDummy.setVisibility(View.GONE);
        linearLayout.removeAllViews();
        for (int i = 0; i < urlList.size(); i++) {

            ImageView imageView = new ImageView(this);
            imageView.setId(i);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("test", "url to be downloaded" + urlList.get(v.getId()));


                    //           Picasso.with(MapActivity.this).load(urlList.get(v.getId())).into(target);





                    /*Picasso.with(MapActivity.this).load(
                            urlList.get(v.getId()).into(target);*/

                }
            });
            imageView.setPadding(2, 2, 2, 2);

            imageView.setScaleType(ImageView.ScaleType.FIT_XY);

            Picasso.with(MapActivity.this)
                    .load(urlList.get(i))
                    .placeholder(net.webonise.nishant.rxjavaretrofeittandem.R.drawable.image_laoding)
                    .error(net.webonise.nishant.rxjavaretrofeittandem.R.drawable.image_error_loading)
                    .resize(200, 200)
                    .centerCrop()
                    .into(imageView);


            linearLayout.addView(imageView);
        }

    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            File file = new File(
                    Environment.getExternalStorageDirectory().getPath()
                            + "/saved.jpg");
            try {
                file.createNewFile();
                FileOutputStream ostream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                ostream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };


    /* @Override
     protected void onPause() {
         super.onPause();
         Picasso.with(this).cancelRequest(target);
         super.onDestroy();

     }
 */
    private void DownLoadImage(String url) {

        Picasso.with(MapActivity.this)
                .load(url)
                .into(new Target() {
                          @Override
                          public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                              try {
                                  String root = Environment.getExternalStorageDirectory().toString();
                                  File myDir = new File(root + "/yourDirectory");

                                  if (!myDir.exists()) {
                                      myDir.mkdirs();
                                  }

                                  String name = new Date().toString() + ".jpg";
                                  myDir = new File(myDir, name);
                                  FileOutputStream out = new FileOutputStream(myDir);
                                  bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);

                                  out.flush();
                                  out.close();
                              } catch (Exception e) {
                                  // some action

                                  Log.e("test", e.getMessage());

                              }
                          }

                          @Override
                          public void onBitmapFailed(Drawable errorDrawable) {
                          }

                          @Override
                          public void onPrepareLoad(Drawable placeHolderDrawable) {
                          }
                      }
                );

    }


    @Override
    public void loadPlaceDetails(PlaceDetailsPojo detailsPojo) {
    }

    @Override
    public void setPlaceImage(List<String> urlList) {
        setImages(urlList);
    }

    @Override
    public void addMarker(LatLng latLng, String title) {
        mSupportMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));//Moves the camera to users current longitude and latitude
        mSupportMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, (float) 10));//
        MarkerOptions marker = new MarkerOptions().position(latLng).title(title);
        mMarker = mSupportMap.addMarker(marker);

    }

    @Override
    public void showToast(String toast) {
        Toast.makeText(getApplicationContext(), toast, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgressDialog() {
        progressDialog = ProgressDialog.show(this, "Please Wait.....", "Loading", true);

    }

    @Override
    public void cancelProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }

    @Override
    public Boolean isMapReady() {
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MapActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Location location = null;
        mSupportMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);

        }
        mSupportMap.setMyLocationEnabled(true);
        mSupportMap.setOnMarkerClickListener(this);
        mSupportMap.setPadding(5, 5, 5, 5);


        location = getLocation();


        if (location != null) {
            LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
            addMarker(latlng, "Current Positions");

            setMarkersOnMap();
        } else {
            Toast.makeText(getApplicationContext(), "Check GPS is ON ?", Toast.LENGTH_LONG).show();
        }


    }

    private void setMarkersOnMap() {
        for (int i = 0; i < mPlacePojo.getResults().size(); i++) {

            Double lat = mPlacePojo.getResults().get(i).getGeometry().getLocation().getLat();
            Double lng = mPlacePojo.getResults().get(i).getGeometry().getLocation().getLng();
            LatLng latLng = new LatLng(lat, lng);
            markerHashMap.put(mPlacePojo.getResults().get(i).getName(), mPlacePojo.getResults().get(i).getPlaceId());
            addMarker(latLng, mPlacePojo.getResults().get(i).getName());
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        String placeId = markerHashMap.get(marker.getTitle());
        mapScreenPresenter.onMarkerClicked(placeId);
        return true;
    }

    public Location getLocation() {

        Location location = null;
        Boolean isNetworkEnabled;
        LocationManager locationManager;
        Criteria criteria = new Criteria();
        //String provider = service.getBestProvider(criteria, false);
        try {
            locationManager = (LocationManager)
                    getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    /*locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);*/
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    Log.d("Network", "Network Enabled");

                    if (locationManager != null) {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            //return sd;


                        }
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        Log.d("GPS", "GPS Enabled");

                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;

    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
