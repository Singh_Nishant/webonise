package net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.presenter;

import android.util.Log;

import net.webonise.nishant.rxjavaretrofeittandem.api.IPlaceService;
import net.webonise.nishant.rxjavaretrofeittandem.api.ServiceFactory;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.PlacePojo;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.view.IMainView;
import net.webonise.nishant.rxjavaretrofeittandem.utils.Constants;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by NISHANT on 30-10-2016.
 */

public class MainPresenter {
    IMainView imview;
    public PlacePojo mResponse;


    public MainPresenter(IMainView imv) {
        imview = imv;
    }

    public void fetchPlaces(String query) {
        IPlaceService service = ServiceFactory.createRetrofitService(IPlaceService.class, IPlaceService.PLACE_ENDPOINT);


        service.getPlaces(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PlacePojo>() {
                    @Override
                    public final void onCompleted() {
                        // do nothing
                        Log.e("test", "onCompleted");
                    }

                    @Override
                    public final void onError(Throwable e) {
                        Log.e("test", e.getMessage());
                        imview.loadPlaces(null);
                    }

                    @Override
                    public final void onNext(PlacePojo response) {
                        if (response.getStatus().equalsIgnoreCase(Constants.OK)) {
                            mResponse = response;
                            Log.e("test", "set the value");
                            imview.loadPlaces(response);
                        } else {
                            imview.showToast(response.getStatus().toString());
                        }


                    }
                });

    }


    public void moveToMapActivity() {
        imview.moveToMapActivity();
    }
}
