package net.webonise.nishant.rxjavaretrofeittandem.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.webonise.nishant.rxjavaretrofeittandem.R;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.Result;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by NISHANT on 30-10-2016.
 */

public class SampleAdapter extends RecyclerView.Adapter<SampleAdapter.ViewHolder> {

    List<Result> mList;
    static Context mContext;

    public SampleAdapter(Context context, List<Result> place) {
        Log.e("nishant", "SampleAdapter" + place.size());
        mContext = context;
        mList = place;
    }

    @Override
    public SampleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View convertView = inflater.inflate(R.layout.cardview_row, parent, false);
        ViewHolder viewholder = new ViewHolder(convertView);
        //convertView.setOnClickListener(this);
        Log.e("nishant", "SampleAdapter:onCreateViewHolder");
        return viewholder;
    }


    @Override
    public void onBindViewHolder(SampleAdapter.ViewHolder holder, int position) {
        Result result = mList.get(position);
        Log.e("nishant", "SampleAdapter:onBindViewHolder" + position);
        holder.title1.setText(result.getName());
        holder.title2.setText(result.getFormattedAddress());
        holder.title3.setText("" + result.getReference());
    }

    @Override
    public int getItemCount() {
        Log.e("nishant", "SampleAdapter:getItemCount" + mList.size());
        return mList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.title1)
        TextView title1;

        @Bind(R.id.title2)
        TextView title2;

        @Bind(R.id.title3)
        TextView title3;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
