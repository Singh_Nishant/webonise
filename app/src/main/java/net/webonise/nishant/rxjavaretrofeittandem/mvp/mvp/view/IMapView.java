package net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.view;

import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.PlaceDetailsPojo;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by NISHANT on 13-11-2016.
 */

public interface IMapView {
    public void loadPlaceDetails(PlaceDetailsPojo detailsPojo);

    public void setPlaceImage(List<String> urlList);

    public void addMarker(LatLng latLng, String title);

    public void showToast(String toast);

    public void showProgressDialog();

    public void cancelProgressDialog();

    public Boolean isMapReady();
}
