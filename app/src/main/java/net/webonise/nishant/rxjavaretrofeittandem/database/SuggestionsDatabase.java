package net.webonise.nishant.rxjavaretrofeittandem.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.R.attr.version;

/**
 * Created by NISHANT on 14-11-2016.
 */

public class SuggestionsDatabase extends SQLiteOpenHelper {

    public static final String DB_SUGGESTION = "SUGGESTION_DB";
    public final static String TABLE_SUGGESTION = "SUGGESTION_TB";
    public final static String FIELD_ID = "_id";
    public final static String FIELD_SUGGESTION = "suggestion";


    public SuggestionsDatabase(Context context) {
        super(context, DB_SUGGESTION, null, version);
    }


    /*public long insertSuggestion(String text) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(FIELD_SUGGESTION, text);
        return db.insert(TABLE_SUGGESTION, null, values);
    }

    public Cursor getSuggestions(String text) {
        SQLiteDatabase db = getWritableDatabase();
        return db.query(TABLE_SUGGESTION, new String[]{FIELD_ID, FIELD_SUGGESTION},
                FIELD_SUGGESTION + " LIKE '" + text + "%'", null, null, null, null);
    }*/


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_SUGGESTION + " (" +
                FIELD_ID + " integer primary key autoincrement, " + FIELD_SUGGESTION + " text UNIQUE);");
        Log.d("SUGGESTION", "DB CREATED");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("test", "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUGGESTION);
        onCreate(db);
    }


}
