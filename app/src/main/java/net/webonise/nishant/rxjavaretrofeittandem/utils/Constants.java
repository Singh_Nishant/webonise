package net.webonise.nishant.rxjavaretrofeittandem.utils;

/**
 * Created by NISHANT on 13-11-2016.
 */

public class Constants {
    public static final String PLACE_ID = "placeID";
    public static final String OK = "ok";
    public static final String LOCATION = "location";
    public static final String POJO_CLASS = "pojo_class";
    public static final String REFERENCE_NAME = "mypref";
    public static final String LOGIN = "login";
}
