package net.webonise.nishant.rxjavaretrofeittandem.api;

import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.PlaceDetailsPojo;
import net.webonise.nishant.rxjavaretrofeittandem.mvp.mvp.model.PlacePojo;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by NISHANT on 30-10-2016.
 */

public interface IPlaceService {
    String PLACE_ENDPOINT = "https://maps.googleapis.com/maps";

    // place api
    @GET("/api/place/textsearch/json?key=AIzaSyCXsZ0H6STsWEAqysQmkWYEY0pgSpWQnUs")
    Observable<PlacePojo> getPlaces(@Query("query") String query);

    //place details api
    @GET("/api/place/details/json?key=AIzaSyCXsZ0H6STsWEAqysQmkWYEY0pgSpWQnUs")
    Observable<PlaceDetailsPojo> getPlaceDetails(@Query("placeid") String placeId);
    // https://maps.googleapis.com/maps/api/place/photo?parameters
    // /api/place/photo?maxwidth=400&photoreference=CnRtAAAATLZNl354RwP_9UKbQ_5Psy40texXePv4oAlgP4qNEkdIrkyse7rPXYGd9D_Uj1rVsQdWT4oRz4QrYAJNpFX7rzqqMlZw2h2E2y5IKMUZ7ouD_SlcHxYq1yL4KbKUv3qtWgTK0A6QbGh87GB3sscrHRIQiG2RrmU_jF4tENr9wGS_YxoUSSDrYjWmrNfeEHSGSc3FyhNLlBU&key=AIzaSyCWly2FtZjMH_b2qGF02w9lJ1_PIxaoSZg


}

